### Requesting a CVE for Your Project?
Please choose the `CVE Request` issue template in the select box above if you
are a maintainer of a *public* project hosted on GitLab.com and you want to
request a CVE identifier.

### Requesting a CVE for GitLab Itself?
**GitLab team members only:** Please choose the `Internal GitLab Submission`
issue template to request a CVE for an issue in GitLab.

**If you believe you have found a security issue in GitLab, you can report it
via the Bug Bounty program on HackerOne: https://hackerone.com/gitlab**

### Something Else?
Feel free to delete this message if you need help with your CVE request, or if
you need to report something to us.


<!-- These quick actions assign correct labels and makes the issue confidential. Please do not edit or remove! -->
/label ~"devops::secure" ~"group::vulnerability research" ~"vulnerability research::advisory"
/confidential
