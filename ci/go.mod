module gitlab.com/gitlab-org/cves

go 1.23.0

require github.com/lmittmann/tint v1.0.5

require github.com/hashicorp/go-cleanhttp v0.5.2 // indirect

require (
	github.com/hashicorp/go-retryablehttp v0.7.7
	gitlab.com/mhenriksen/gittest v0.0.0-20240828124509-2f300e2da600
)
