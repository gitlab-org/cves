package main

import (
	"os"

	"gitlab.com/gitlab-org/cves/internal/cli"
)

func main() {
	os.Exit(cli.MitreSync(os.Stdout, os.Args[1:]))
}
