package cve

// cve-schema specifies the CVE JSON record format. This is the blueprint for a rich set of
// JSON data that can be submitted by CVE Numbering Authorities (CNAs) and Authorized Data
// Publishers (ADPs) to describe a CVE Record. Some examples of CVE Record data include CVE
// ID number, affected product(s), affected version(s), and public references. While those
// specific items are required when assigning a CVE, there are many other optional data in
// the schema that can be used to enrich CVE Records for community benefit. Learn more about
// the CVE program at [the official website](https://cve.mitre.org). This CVE JSON record
// format is defined using JSON Schema. Learn more about JSON Schema
// [here](https://json-schema.org/).
//
// When a CNA populates the data associated with a CVE ID as a CVE Record, the state of the
// CVE Record is Published.
//
// If the CVE ID and associated CVE Record should no longer be used, the CVE Record is
// placed in the Rejected state. A Rejected CVE Record remains on the CVE List so that users
// can know when it is invalid.
type Record struct {
	// A set of structures (called containers) used to store vulnerability information related             
	// to a specific CVE ID provided by a specific organization participating in the CVE                   
	// program. Each container includes information provided by a different source.                        
	//                                                                                                     
	// At a minimum, a 'cna' container containing the vulnerability information provided by the            
	// CNA who initially assigned the CVE ID must be included.                                             
	//                                                                                                     
	// There can only be one 'cna' container, as there can only be one assigning CNA. However,             
	// there can be multiple 'adp' containers, allowing multiple organizations participating in            
	// the CVE program to add additional information related to the vulnerability. For the most            
	// part, the 'cna' and 'adp' containers contain the same properties. The main differences              
	// are the source of the information. The 'cna' container requires the CNA to include                  
	// certain fields, while the 'adp' container does not.                                                 
	//                                                                                                     
	// A set of structures (called containers) used to store vulnerability information related             
	// to a specific CVE ID provided by a specific organization participating in the CVE                   
	// program. Each container includes information provided by a different source.                        
	//                                                                                                     
	// At minimum, a 'cna' container containing the vulnerability information provided by the              
	// CNA who initially assigned the CVE ID must be included.                                             
	//                                                                                                     
	// There can only be one 'cna' container, as there can only be one assigning CNA.                      
	Containers                                                                                 Containers  `json:"containers"`
	CveMetadata                                                                                CveMetadata `json:"cveMetadata"`
	DataType                                                                                   DataType    `json:"dataType"`
	DataVersion                                                                                string      `json:"dataVersion"`
}

// A set of structures (called containers) used to store vulnerability information related
// to a specific CVE ID provided by a specific organization participating in the CVE
// program. Each container includes information provided by a different source.
//
// At a minimum, a 'cna' container containing the vulnerability information provided by the
// CNA who initially assigned the CVE ID must be included.
//
// There can only be one 'cna' container, as there can only be one assigning CNA. However,
// there can be multiple 'adp' containers, allowing multiple organizations participating in
// the CVE program to add additional information related to the vulnerability. For the most
// part, the 'cna' and 'adp' containers contain the same properties. The main differences
// are the source of the information. The 'cna' container requires the CNA to include
// certain fields, while the 'adp' container does not.
//
// A set of structures (called containers) used to store vulnerability information related
// to a specific CVE ID provided by a specific organization participating in the CVE
// program. Each container includes information provided by a different source.
//
// At minimum, a 'cna' container containing the vulnerability information provided by the
// CNA who initially assigned the CVE ID must be included.
//
// There can only be one 'cna' container, as there can only be one assigning CNA.
type Containers struct {
	Adp []AdpContainer `json:"adp,omitempty"`
	Cna CnaEdContainer `json:"cna"`
}

// An object containing the vulnerability information provided by an Authorized Data
// Publisher (ADP). Since multiple ADPs can provide information for a CVE ID, an ADP
// container must indicate which ADP is the source of the information in the object.
type AdpContainer struct {
	Affected                                                                                []Product              `json:"affected,omitempty"`
	Configurations                                                                          []ConfigurationElement `json:"configurations,omitempty"`
	Credits                                                                                 []Credit               `json:"credits,omitempty"`
	// If known, the date/time the vulnerability was disclosed publicly.                                           
	DatePublic                                                                              *string                `json:"datePublic,omitempty"`
	Descriptions                                                                            []ConfigurationElement `json:"descriptions,omitempty"`
	Exploits                                                                                []ConfigurationElement `json:"exploits,omitempty"`
	Impacts                                                                                 []Impact               `json:"impacts,omitempty"`
	Metrics                                                                                 []Metric               `json:"metrics,omitempty"`
	ProblemTypes                                                                            []ProblemType          `json:"problemTypes,omitempty"`
	ProviderMetadata                                                                        ProviderMetadata       `json:"providerMetadata"`
	References                                                                              []Reference            `json:"references,omitempty"`
	Solutions                                                                               []ConfigurationElement `json:"solutions,omitempty"`
	Source                                                                                  map[string]interface{} `json:"source,omitempty"`
	Tags                                                                                    []string               `json:"tags,omitempty"`
	TaxonomyMappings                                                                        []TaxonomyMapping      `json:"taxonomyMappings,omitempty"`
	Timeline                                                                                []Timeline             `json:"timeline,omitempty"`
	// A title, headline, or a brief phrase summarizing the information in an ADP container.                       
	Title                                                                                   *string                `json:"title,omitempty"`
	Workarounds                                                                             []ConfigurationElement `json:"workarounds,omitempty"`
}

// List of affected products.
//
// Provides information about the set of products and services affected by this
// vulnerability.
type Product struct {
	// URL identifying a package collection (determines the meaning of packageName).                                          
	CollectionURL                                                                                            *string          `json:"collectionURL,omitempty"`
	// Affected products defined by CPE. This is an array of CPE values (vulnerable and not), we                              
	// use an array so that we can make multiple statements about the same version and they are                               
	// separate (if we used a JSON object we'd essentially be keying on the CPE name and they                                 
	// would have to overlap). Also, this allows things like cveDataVersion or cveDescription to                              
	// be applied directly to the product entry. This also allows more complex statements such                                
	// as "Product X between versions 10.2 and 10.8" to be put in a machine-readable format. As                               
	// well since multiple statements can be used multiple branches of the same product can be                                
	// defined here.                                                                                                          
	Cpes                                                                                                     []string         `json:"cpes,omitempty"`
	// The default status for versions that are not otherwise listed in the versions list. If                                 
	// not specified, defaultStatus defaults to 'unknown'. Versions or defaultStatus may be                                   
	// omitted, but not both.                                                                                                 
	DefaultStatus                                                                                            *Status          `json:"defaultStatus,omitempty"`
	// A list of the affected components, features, modules, sub-components, sub-products, APIs,                              
	// commands, utilities, programs, or functionalities (optional).                                                          
	Modules                                                                                                  []string         `json:"modules,omitempty"`
	// Name or identifier of the affected software package as used in the package collection.                                 
	PackageName                                                                                              *string          `json:"packageName,omitempty"`
	// List of specific platforms if the vulnerability is only relevant in the context of these                               
	// platforms (optional). Platforms may include execution environments, operating systems,                                 
	// virtualization technologies, hardware models, or computing architectures. The lack of                                  
	// this field or an empty array implies that the other fields are applicable to all relevant                              
	// platforms.                                                                                                             
	Platforms                                                                                                []string         `json:"platforms,omitempty"`
	// Name of the affected product.                                                                                          
	Product                                                                                                  *string          `json:"product,omitempty"`
	// A list of the affected source code files (optional).                                                                   
	ProgramFiles                                                                                             []string         `json:"programFiles,omitempty"`
	// A list of the affected source code functions, methods, subroutines, or procedures                                      
	// (optional).                                                                                                            
	ProgramRoutines                                                                                          []ProgramRoutine `json:"programRoutines,omitempty"`
	// The URL of the source code repository, for informational purposes and/or to resolve git                                
	// hash version ranges.                                                                                                   
	Repo                                                                                                     *string          `json:"repo,omitempty"`
	// Name of the organization, project, community, individual, or user that created or                                      
	// maintains this product or hosted service. Can be 'N/A' if none of those apply. When                                    
	// collectionURL and packageName are used, this field may optionally represent the user or                                
	// account within the package collection associated with the package.                                                     
	Vendor                                                                                                   *string          `json:"vendor,omitempty"`
	// Set of product versions or version ranges related to the vulnerability. The versions                                   
	// satisfy the CNA Rules [8.1.2                                                                                           
	// requirement](https://cve.mitre.org/cve/cna/rules.html#section_8-1_cve_entry_information_requirements).                 
	// Versions or defaultStatus may be omitted, but not both.                                                                
	Versions                                                                                                 []VersionElement `json:"versions,omitempty"`
}

// An object describing program routine.
type ProgramRoutine struct {
	// Name of the affected source code file, function, method, subroutine, or procedure.       
	Name                                                                                 string `json:"name"`
}

// A single version or a range of versions, with vulnerability status.
//
// An entry with only 'version' and 'status' indicates the status of a single version.
//
// Otherwise, an entry describes a range; it must include the 'versionType' property, to
// define the version numbering semantics in use, and 'limit', to indicate the non-inclusive
// upper limit of the range. The object describes the status for versions V such that
// 'version' <= V and V < 'limit', using the <= and < semantics defined for the specific
// kind of 'versionType'. Status changes within the range can be specified by an optional
// 'changes' list.
//
// The algorithm to decide the status specified for a version V is:
//
// for entry in product.versions {
// if entry.lessThan is not present and entry.lessThanOrEqual is not present and v ==
// entry.version {
// return entry.status
// }
// if (entry.lessThan is present and entry.version <= v and v < entry.lessThan)
// or
// (entry.lessThanOrEqual is present and entry.version <= v and v <= entry.lessThanOrEqual)
// { // <= and < defined by entry.versionType
// status = entry.status
// for change in entry.changes {
// if change.at <= v {
// status = change.status
// }
// }
// return status
// }
// }
// return product.defaultStatus
//
// .
type VersionElement struct {
	// A list of status changes that take place during the range. The array should be sorted in          
	// increasing order by the 'at' field, according to the versionType, but clients must                
	// re-sort the list themselves rather than assume it is sorted.                                      
	Changes                                                                                     []Change `json:"changes,omitempty"`
	// The non-inclusive upper limit of the range. This is the least version NOT in the range.           
	// The usual version syntax is expanded to allow a pattern to end in an asterisk `(*)`,              
	// indicating an arbitrarily large number in the version ordering. For example, `{version:           
	// 1.0 lessThan: 1.*}` would describe the entire 1.X branch for most range kinds, and                
	// `{version: 2.0, lessThan: *}` describes all versions starting at 2.0, including 3.0, 5.1,         
	// and so on. Only one of lessThan and lessThanOrEqual should be specified.                          
	LessThan                                                                                    *string  `json:"lessThan,omitempty"`
	// The inclusive upper limit of the range. This is the greatest version contained in the             
	// range. Only one of lessThan and lessThanOrEqual should be specified. For example,                 
	// `{version: 1.0, lessThanOrEqual: 1.3}` covers all versions from 1.0 up to and including           
	// 1.3.                                                                                              
	LessThanOrEqual                                                                             *string  `json:"lessThanOrEqual,omitempty"`
	// The vulnerability status for the version or range of versions. For a range, the status            
	// may be refined by the 'changes' list.                                                             
	Status                                                                                      Status   `json:"status"`
	// The single version being described, or the version at the start of the range. By                  
	// convention, typically 0 denotes the earliest possible version.                                    
	Version                                                                                     string   `json:"version"`
	// The version numbering system used for specifying the range. This defines the exact                
	// semantics of the comparison (less-than) operation on versions, which is required to               
	// understand the range itself. 'Custom' indicates that the version type is unspecified and          
	// should be avoided whenever possible. It is included primarily for use in conversion of            
	// older data files.                                                                                 
	VersionType                                                                                 *string  `json:"versionType,omitempty"`
}

// The start of a single status change during the range.
type Change struct {
	// The version at which a status change occurs.                     
	At                                                           string `json:"at"`
	// The new status in the range starting at the given version.       
	Status                                                       Status `json:"status"`
}

// Configurations required for exploiting this vulnerability.
//
// Text in a particular language with optional alternate markup or formatted representation
// (e.g., Markdown) or embedded media.
//
// A list of multi-lingual descriptions of the vulnerability. E.g., [PROBLEMTYPE] in
// [COMPONENT] in [VENDOR] [PRODUCT] [VERSION] on [PLATFORMS] allows [ATTACKER] to [IMPACT]
// via [VECTOR]. OR [COMPONENT] in [VENDOR] [PRODUCT] [VERSION] [ROOT CAUSE], which allows
// [ATTACKER] to [IMPACT] via [VECTOR].
//
// Information about exploits of the vulnerability.
//
// Information about solutions or remediations available for this vulnerability.
//
// Workarounds and mitigations for this vulnerability.
type ConfigurationElement struct {
	Lang                                                                                        string            `json:"lang"`
	// Supporting media data for the description such as markdown, diagrams, .. (optional).                       
	// Similar to RFC 2397 each media object has three main parts: media type, media data value,                  
	// and an optional boolean flag to indicate if the media data is base64 encoded.                              
	SupportingMedia                                                                             []SupportingMedia `json:"supportingMedia,omitempty"`
	// Plain text description.                                                                                    
	Value                                                                                       string            `json:"value"`
}

type SupportingMedia struct {
	// If true then the value field contains the media data encoded in base64. If false then the       
	// value field contains the UTF-8 media content.                                                   
	Base64                                                                                      *bool  `json:"base64,omitempty"`
	// RFC2046 compliant IANA Media type for eg., text/markdown, text/html.                            
	Type                                                                                        string `json:"type"`
	// Supporting media content, up to 16K. If base64 is true, this field stores base64 encoded        
	// data.                                                                                           
	Value                                                                                       string `json:"value"`
}

// Statements acknowledging specific people, organizations, or tools recognizing the work
// done in researching, discovering, remediating or helping with activities related to this
// CVE.
type Credit struct {
	// The language used when describing the credits. The language field is included so that CVE        
	// Records can support translations. The value must be a BCP 47 language code.                      
	Lang                                                                                        string  `json:"lang"`
	// Type or role of the entity being credited (optional). finder: identifies the                     
	// vulnerability.                                                                                   
	// reporter: notifies the vendor of the vulnerability to a CNA.                                     
	// analyst: validates the vulnerability to ensure accuracy or severity.                             
	// coordinator: facilitates the coordinated response process.                                       
	// remediation developer: prepares a code change or other remediation plans.                        
	// remediation reviewer: reviews vulnerability remediation plans or code changes for                
	// effectiveness and completeness.                                                                  
	// remediation verifier: tests and verifies the vulnerability or its remediation.                   
	// tool: names of tools used in vulnerability discovery or identification.                          
	// sponsor: supports the vulnerability identification or remediation activities.                    
	Type                                                                                        *Type   `json:"type,omitempty"`
	// UUID of the user being credited if present in the CVE User Registry (optional). This UUID        
	// can be used to lookup the user record in the user registry service.                              
	User                                                                                        *string `json:"user,omitempty"`
	Value                                                                                       string  `json:"value"`
}

// Collection of impacts of this vulnerability.
//
// This is impact type information (e.g. a text description.
type Impact struct {
	// CAPEC ID that best relates to this impact.                                                                    
	CapecID                                                                                   *string                `json:"capecId,omitempty"`
	// Prose description of the impact scenario. At a minimum provide the description given by                       
	// CAPEC.                                                                                                        
	Descriptions                                                                              []ConfigurationElement `json:"descriptions"`
}

// Collection of impact scores with attribution.
//
// This is impact type information (e.g. a text description, CVSSv2, CVSSv3, CVSSV4, etc.).
// Must contain: At least one entry, can be text, CVSSv2, CVSSv3, others may be added.
type Metric struct {
	CvssV20                                                                                     *JSONSchemaForCommonVulnerabilityScoringSystemVersion20 `json:"cvssV2_0,omitempty"`
	CvssV30                                                                                     *JSONSchemaForCommonVulnerabilityScoringSystemVersion30 `json:"cvssV3_0,omitempty"`
	CvssV31                                                                                     *JSONSchemaForCommonVulnerabilityScoringSystemVersion31 `json:"cvssV3_1,omitempty"`
	CvssV40                                                                                     *JSONSchemaForCommonVulnerabilityScoringSystemVersion40 `json:"cvssV4_0,omitempty"`
	// Name of the scoring format. This provides a bit of future proofing. Additional properties                                                        
	// are not prohibited, so this will support the inclusion of proprietary formats. It also                                                           
	// provides an easy future conversion mechanism when future score formats become part of the                                                        
	// schema. example: cvssV44, format = 'cvssV44', other = cvssV4_4 JSON object. In the                                                               
	// future, the other properties can be converted to score properties when they become part                                                          
	// of the schema.                                                                                                                                   
	Format                                                                                      *string                                                 `json:"format,omitempty"`
	// A non-standard impact description, may be prose or JSON block.                                                                                   
	Other                                                                                       *OtherClass                                             `json:"other,omitempty"`
	// Description of the scenarios this metrics object applies to. If no specific scenario is                                                          
	// given, GENERAL is used as the default and applies when no more specific metric matches.                                                          
	Scenarios                                                                                   []Scenario                                              `json:"scenarios,omitempty"`
}

type JSONSchemaForCommonVulnerabilityScoringSystemVersion20 struct {
	AccessComplexity           *AccessComplexityType          `json:"accessComplexity,omitempty"`
	AccessVector               *AccessVectorType              `json:"accessVector,omitempty"`
	Authentication             *AuthenticationType            `json:"authentication,omitempty"`
	AvailabilityImpact         *CiaType                       `json:"availabilityImpact,omitempty"`
	AvailabilityRequirement    *CiaRequirementType            `json:"availabilityRequirement,omitempty"`
	BaseScore                  float64                        `json:"baseScore"`
	CollateralDamagePotential  *CollateralDamagePotentialType `json:"collateralDamagePotential,omitempty"`
	ConfidentialityImpact      *CiaType                       `json:"confidentialityImpact,omitempty"`
	ConfidentialityRequirement *CiaRequirementType            `json:"confidentialityRequirement,omitempty"`
	EnvironmentalScore         *float64                       `json:"environmentalScore,omitempty"`
	Exploitability             *ExploitityType                `json:"exploitability,omitempty"`
	IntegrityImpact            *CiaType                       `json:"integrityImpact,omitempty"`
	IntegrityRequirement       *CiaRequirementType            `json:"integrityRequirement,omitempty"`
	RemediationLevel           *RemediationLevelType          `json:"remediationLevel,omitempty"`
	ReportConfidence           *ReportConfidenceType          `json:"reportConfidence,omitempty"`
	TargetDistribution         *TargetDistributionType        `json:"targetDistribution,omitempty"`
	TemporalScore              *float64                       `json:"temporalScore,omitempty"`
	VectorString               string                         `json:"vectorString"`
	// CVSS Version                                           
	Version                    CvssV20_Version                `json:"version"`
}

type JSONSchemaForCommonVulnerabilityScoringSystemVersion30 struct {
	AttackComplexity              *AttackComplexityType                                                           `json:"attackComplexity,omitempty"`
	AttackVector                  *JSONSchemaForCommonVulnerabilityScoringSystemVersion30_AttackVector            `json:"attackVector,omitempty"`
	AvailabilityImpact            *AvailabilityImpactEnum                                                         `json:"availabilityImpact,omitempty"`
	AvailabilityRequirement       *CiaRequirementType                                                             `json:"availabilityRequirement,omitempty"`
	BaseScore                     float64                                                                         `json:"baseScore"`
	BaseSeverity                  SeverityType                                                                    `json:"baseSeverity"`
	ConfidentialityImpact         *AvailabilityImpactEnum                                                         `json:"confidentialityImpact,omitempty"`
	ConfidentialityRequirement    *CiaRequirementType                                                             `json:"confidentialityRequirement,omitempty"`
	EnvironmentalScore            *float64                                                                        `json:"environmentalScore,omitempty"`
	EnvironmentalSeverity         *SeverityType                                                                   `json:"environmentalSeverity,omitempty"`
	ExploitCodeMaturity           *ExploitityType                                                                 `json:"exploitCodeMaturity,omitempty"`
	IntegrityImpact               *AvailabilityImpactEnum                                                         `json:"integrityImpact,omitempty"`
	IntegrityRequirement          *CiaRequirementType                                                             `json:"integrityRequirement,omitempty"`
	ModifiedAttackComplexity      *ModifiedAttackComplexityType                                                   `json:"modifiedAttackComplexity,omitempty"`
	ModifiedAttackVector          *JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedAttackVector    `json:"modifiedAttackVector,omitempty"`
	ModifiedAvailabilityImpact    *ModifiedType                                                                   `json:"modifiedAvailabilityImpact,omitempty"`
	ModifiedConfidentialityImpact *ModifiedType                                                                   `json:"modifiedConfidentialityImpact,omitempty"`
	ModifiedIntegrityImpact       *ModifiedType                                                                   `json:"modifiedIntegrityImpact,omitempty"`
	ModifiedPrivilegesRequired    *ModifiedType                                                                   `json:"modifiedPrivilegesRequired,omitempty"`
	ModifiedScope                 *ModifiedScopeType                                                              `json:"modifiedScope,omitempty"`
	ModifiedUserInteraction       *JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedUserInteraction `json:"modifiedUserInteraction,omitempty"`
	PrivilegesRequired            *AvailabilityImpactEnum                                                         `json:"privilegesRequired,omitempty"`
	RemediationLevel              *RemediationLevelType                                                           `json:"remediationLevel,omitempty"`
	ReportConfidence              *ConfidenceType                                                                 `json:"reportConfidence,omitempty"`
	Scope                         *ScopeType                                                                      `json:"scope,omitempty"`
	TemporalScore                 *float64                                                                        `json:"temporalScore,omitempty"`
	TemporalSeverity              *SeverityType                                                                   `json:"temporalSeverity,omitempty"`
	UserInteraction               *JSONSchemaForCommonVulnerabilityScoringSystemVersion30_UserInteraction         `json:"userInteraction,omitempty"`
	VectorString                  string                                                                          `json:"vectorString"`
	// CVSS Version                                                                                               
	Version                       JSONSchemaForCommonVulnerabilityScoringSystemVersion30_Version                  `json:"version"`
}

type JSONSchemaForCommonVulnerabilityScoringSystemVersion31 struct {
	AttackComplexity              *AttackComplexityType                                                           `json:"attackComplexity,omitempty"`
	AttackVector                  *JSONSchemaForCommonVulnerabilityScoringSystemVersion30_AttackVector            `json:"attackVector,omitempty"`
	AvailabilityImpact            *AvailabilityImpactEnum                                                         `json:"availabilityImpact,omitempty"`
	AvailabilityRequirement       *CiaRequirementType                                                             `json:"availabilityRequirement,omitempty"`
	BaseScore                     float64                                                                         `json:"baseScore"`
	BaseSeverity                  SeverityType                                                                    `json:"baseSeverity"`
	ConfidentialityImpact         *AvailabilityImpactEnum                                                         `json:"confidentialityImpact,omitempty"`
	ConfidentialityRequirement    *CiaRequirementType                                                             `json:"confidentialityRequirement,omitempty"`
	EnvironmentalScore            *float64                                                                        `json:"environmentalScore,omitempty"`
	EnvironmentalSeverity         *SeverityType                                                                   `json:"environmentalSeverity,omitempty"`
	ExploitCodeMaturity           *ExploitityType                                                                 `json:"exploitCodeMaturity,omitempty"`
	IntegrityImpact               *AvailabilityImpactEnum                                                         `json:"integrityImpact,omitempty"`
	IntegrityRequirement          *CiaRequirementType                                                             `json:"integrityRequirement,omitempty"`
	ModifiedAttackComplexity      *ModifiedAttackComplexityType                                                   `json:"modifiedAttackComplexity,omitempty"`
	ModifiedAttackVector          *JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedAttackVector    `json:"modifiedAttackVector,omitempty"`
	ModifiedAvailabilityImpact    *ModifiedType                                                                   `json:"modifiedAvailabilityImpact,omitempty"`
	ModifiedConfidentialityImpact *ModifiedType                                                                   `json:"modifiedConfidentialityImpact,omitempty"`
	ModifiedIntegrityImpact       *ModifiedType                                                                   `json:"modifiedIntegrityImpact,omitempty"`
	ModifiedPrivilegesRequired    *ModifiedType                                                                   `json:"modifiedPrivilegesRequired,omitempty"`
	ModifiedScope                 *ModifiedScopeType                                                              `json:"modifiedScope,omitempty"`
	ModifiedUserInteraction       *JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedUserInteraction `json:"modifiedUserInteraction,omitempty"`
	PrivilegesRequired            *AvailabilityImpactEnum                                                         `json:"privilegesRequired,omitempty"`
	RemediationLevel              *RemediationLevelType                                                           `json:"remediationLevel,omitempty"`
	ReportConfidence              *ConfidenceType                                                                 `json:"reportConfidence,omitempty"`
	Scope                         *ScopeType                                                                      `json:"scope,omitempty"`
	TemporalScore                 *float64                                                                        `json:"temporalScore,omitempty"`
	TemporalSeverity              *SeverityType                                                                   `json:"temporalSeverity,omitempty"`
	UserInteraction               *JSONSchemaForCommonVulnerabilityScoringSystemVersion30_UserInteraction         `json:"userInteraction,omitempty"`
	VectorString                  string                                                                          `json:"vectorString"`
	// CVSS Version                                                                                               
	Version                       JSONSchemaForCommonVulnerabilityScoringSystemVersion31_Version                  `json:"version"`
}

type JSONSchemaForCommonVulnerabilityScoringSystemVersion40 struct {
	AttackComplexity                  *AttackComplexityType                                                           `json:"attackComplexity,omitempty"`
	AttackRequirements                *AttackRequirementsType                                                         `json:"attackRequirements,omitempty"`
	AttackVector                      *JSONSchemaForCommonVulnerabilityScoringSystemVersion40_AttackVector            `json:"attackVector,omitempty"`
	Automatable                       *AutomatableType                                                                `json:"Automatable,omitempty"`
	AvailabilityRequirement           *CiaRequirementType                                                             `json:"availabilityRequirement,omitempty"`
	BaseScore                         float64                                                                         `json:"baseScore"`
	BaseSeverity                      SeverityType                                                                    `json:"baseSeverity"`
	ConfidentialityRequirement        *CiaRequirementType                                                             `json:"confidentialityRequirement,omitempty"`
	ExploitMaturity                   *ExploitMaturityType                                                            `json:"exploitMaturity,omitempty"`
	IntegrityRequirement              *CiaRequirementType                                                             `json:"integrityRequirement,omitempty"`
	ModifiedAttackComplexity          *ModifiedAttackComplexityType                                                   `json:"modifiedAttackComplexity,omitempty"`
	ModifiedAttackRequirements        *ModifiedAttackRequirementsType                                                 `json:"modifiedAttackRequirements,omitempty"`
	ModifiedAttackVector              *JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedAttackVector    `json:"modifiedAttackVector,omitempty"`
	ModifiedPrivilegesRequired        *ModifiedType                                                                   `json:"modifiedPrivilegesRequired,omitempty"`
	ModifiedSubAvailabilityImpact     *ModifiedSubIaType                                                              `json:"modifiedSubAvailabilityImpact,omitempty"`
	ModifiedSubConfidentialityImpact  *ModifiedType                                                                   `json:"modifiedSubConfidentialityImpact,omitempty"`
	ModifiedSubIntegrityImpact        *ModifiedSubIaType                                                              `json:"modifiedSubIntegrityImpact,omitempty"`
	ModifiedUserInteraction           *JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedUserInteraction `json:"modifiedUserInteraction,omitempty"`
	ModifiedVulnAvailabilityImpact    *ModifiedType                                                                   `json:"modifiedVulnAvailabilityImpact,omitempty"`
	ModifiedVulnConfidentialityImpact *ModifiedType                                                                   `json:"modifiedVulnConfidentialityImpact,omitempty"`
	ModifiedVulnIntegrityImpact       *ModifiedType                                                                   `json:"modifiedVulnIntegrityImpact,omitempty"`
	PrivilegesRequired                *AvailabilityImpactEnum                                                         `json:"privilegesRequired,omitempty"`
	ProviderUrgency                   *ProviderUrgencyType                                                            `json:"providerUrgency,omitempty"`
	Recovery                          *RecoveryType                                                                   `json:"Recovery,omitempty"`
	Safety                            *SafetyType                                                                     `json:"Safety,omitempty"`
	SubAvailabilityImpact             *AvailabilityImpactEnum                                                         `json:"subAvailabilityImpact,omitempty"`
	SubConfidentialityImpact          *AvailabilityImpactEnum                                                         `json:"subConfidentialityImpact,omitempty"`
	SubIntegrityImpact                *AvailabilityImpactEnum                                                         `json:"subIntegrityImpact,omitempty"`
	UserInteraction                   *JSONSchemaForCommonVulnerabilityScoringSystemVersion40_UserInteraction         `json:"userInteraction,omitempty"`
	ValueDensity                      *ValueDensityType                                                               `json:"valueDensity,omitempty"`
	VectorString                      string                                                                          `json:"vectorString"`
	// CVSS Version                                                                                                   
	Version                           JSONSchemaForCommonVulnerabilityScoringSystemVersion40_Version                  `json:"version"`
	VulnAvailabilityImpact            *AvailabilityImpactEnum                                                         `json:"vulnAvailabilityImpact,omitempty"`
	VulnConfidentialityImpact         *AvailabilityImpactEnum                                                         `json:"vulnConfidentialityImpact,omitempty"`
	VulnerabilityResponseEffort       *VulnerabilityResponseEffortType                                                `json:"vulnerabilityResponseEffort,omitempty"`
	VulnIntegrityImpact               *AvailabilityImpactEnum                                                         `json:"vulnIntegrityImpact,omitempty"`
}

// A non-standard impact description, may be prose or JSON block.
type OtherClass struct {
	// JSON object not covered by another metrics format.                         
	Content                                                map[string]interface{} `json:"content"`
	// Name of the non-standard impact metrics format used.                       
	Type                                                   string                 `json:"type"`
}

type Scenario struct {
	Lang                                                                                      string `json:"lang"`
	// Description of the scenario this metrics object applies to. If no specific scenario is        
	// given, GENERAL is used as the default and applies when no more specific metric matches.       
	Value                                                                                     string `json:"value"`
}

// This is problem type information (e.g. CWE identifier). Must contain: At least one entry,
// can be text, OWASP, CWE, please note that while only one is required you can use more
// than one (or indeed all three) as long as they are correct). (CNA requirement:
// [PROBLEMTYPE]).
type ProblemType struct {
	Descriptions []ProblemTypeDescription `json:"descriptions"`
}

type ProblemTypeDescription struct {
	// CWE ID of the CWE that best describes this problemType entry.            
	CweID                                                           *string     `json:"cweId,omitempty"`
	// Text description of problemType, or title from CWE or OWASP.             
	Description                                                     string      `json:"description"`
	Lang                                                            string      `json:"lang"`
	References                                                      []Reference `json:"references,omitempty"`
	// Problemtype source, text, OWASP, CWE, etc.,                              
	Type                                                            *string     `json:"type,omitempty"`
}

// This is reference data in the form of URLs or file objects (uuencoded and embedded within
// the JSON file, exact format to be decided, e.g. we may require a compressed format so the
// objects require unpacking before they are "dangerous").
type Reference struct {
	// User created name for the reference, often the title of the page.                              
	Name                                                                                     *string  `json:"name,omitempty"`
	// An array of one or more tags that describe the resource referenced by 'url'.                   
	Tags                                                                                     []string `json:"tags,omitempty"`
	// The uniform resource locator (URL), according to [RFC                                          
	// 3986](https://tools.ietf.org/html/rfc3986#section-1.1.3), that can be used to retrieve         
	// the referenced resource.                                                                       
	URL                                                                                      string   `json:"url"`
}

// Details related to the information container provider (CNA or ADP).
type ProviderMetadata struct {
	// Timestamp to be set by the system of record at time of submission. If dateUpdated is             
	// provided to the system of record it will be replaced by the current timestamp at the time        
	// of submission.                                                                                   
	DateUpdated                                                                                 *string `json:"dateUpdated,omitempty"`
	// The container provider's organizational UUID.                                                    
	OrgID                                                                                       string  `json:"orgId"`
	// The container provider's organizational short name.                                              
	ShortName                                                                                   *string `json:"shortName,omitempty"`
}

// List of taxonomy items related to the vulnerability.
//
// A taxonomy mapping object identifies the taxonomy by a name and version (eg., ATT&CK
// v13.1, CVSS 3.1, CWE 4.12) along with a list of relations relevant to this CVE.
type TaxonomyMapping struct {
	// The name of the taxonomy, eg., ATT&CK, D3FEND, CWE, CVSS                       
	TaxonomyName                                                   string             `json:"taxonomyName"`
	// List of relationships to the taxonomy for the vulnerability.                   
	TaxonomyRelations                                              []TaxonomyRelation `json:"taxonomyRelations"`
	// The version of taxonomy the identifiers come from.                             
	TaxonomyVersion                                                *string            `json:"taxonomyVersion,omitempty"`
}

// A relationship between the taxonomy and the CVE or two taxonomy items.
type TaxonomyRelation struct {
	// A description of the relationship.                                                       
	RelationshipName                                                                     string `json:"relationshipName"`
	// The target of the relationship.  Can be the CVE ID or another taxonomy identifier.       
	RelationshipValue                                                                    string `json:"relationshipValue"`
	// Identifier of the item in the taxonomy.  Used as the subject of the relationship.        
	TaxonomyID                                                                           string `json:"taxonomyId"`
}

// This is timeline information for significant events about this vulnerability or changes
// to the CVE Record.
type Timeline struct {
	// The language used in the description of the event. The language field is included so that       
	// CVE Records can support translations. The value must be a BCP 47 language code.                 
	Lang                                                                                        string `json:"lang"`
	// Timestamp representing when the event in the timeline occurred. The timestamp format is         
	// based on RFC3339 and ISO ISO8601, with an optional timezone. yyyy-MM-ddTHH:mm:ss[+-]ZH:ZM       
	// - if the timezone offset is not given, GMT (+00:00) is assumed.                                 
	Time                                                                                        string `json:"time"`
	// A summary of the event.                                                                         
	Value                                                                                       string `json:"value"`
}

// An object containing the vulnerability information provided by a CVE Numbering Authority
// (CNA) for a published CVE ID. There can only be one CNA container per CVE record since
// there can only be one assigning CNA. The CNA container must include the required
// information defined in the CVE Rules, which includes a product, version, problem type,
// prose description, and a reference.
//
// An object containing the vulnerability information provided by a CVE Numbering Authority
// (CNA) for a rejected CVE ID. There can only be one CNA container per CVE record since
// there can only be one assigning CNA.
type CnaEdContainer struct {
	Affected                                                                                   []Product              `json:"affected,omitempty"`
	Configurations                                                                             []ConfigurationElement `json:"configurations,omitempty"`
	Credits                                                                                    []Credit               `json:"credits,omitempty"`
	// The date/time this CVE ID was associated with a vulnerability by a CNA.                                        
	DateAssigned                                                                               *string                `json:"dateAssigned,omitempty"`
	// If known, the date/time the vulnerability was disclosed publicly.                                              
	DatePublic                                                                                 *string                `json:"datePublic,omitempty"`
	Descriptions                                                                               []ConfigurationElement `json:"descriptions,omitempty"`
	Exploits                                                                                   []ConfigurationElement `json:"exploits,omitempty"`
	Impacts                                                                                    []Impact               `json:"impacts,omitempty"`
	Metrics                                                                                    []Metric               `json:"metrics,omitempty"`
	ProblemTypes                                                                               []ProblemType          `json:"problemTypes,omitempty"`
	ProviderMetadata                                                                           ProviderMetadata       `json:"providerMetadata"`
	References                                                                                 []Reference            `json:"references,omitempty"`
	Solutions                                                                                  []ConfigurationElement `json:"solutions,omitempty"`
	Source                                                                                     map[string]interface{} `json:"source,omitempty"`
	Tags                                                                                       []string               `json:"tags,omitempty"`
	TaxonomyMappings                                                                           []TaxonomyMapping      `json:"taxonomyMappings,omitempty"`
	Timeline                                                                                   []Timeline             `json:"timeline,omitempty"`
	// A title, headline, or a brief phrase summarizing the CVE record. Eg., Buffer overflow in                       
	// Example Soft.                                                                                                  
	Title                                                                                      *string                `json:"title,omitempty"`
	Workarounds                                                                                []ConfigurationElement `json:"workarounds,omitempty"`
	// Reasons for rejecting this CVE Record.                                                                         
	RejectedReasons                                                                            []ConfigurationElement `json:"rejectedReasons,omitempty"`
	// Contains an array of CVE IDs that this CVE ID was rejected in favor of because this CVE                        
	// ID was assigned to the vulnerabilities.                                                                        
	ReplacedBy                                                                                 []string               `json:"replacedBy,omitempty"`
}

// This is meta data about the CVE ID such as the CVE ID, who requested it, who assigned it,
// when it was requested, the current state (PUBLISHED, REJECTED, etc.) and so on.  These
// fields are controlled by the CVE Services.
type CveMetadata struct {
	// The UUID for the organization to which the CVE ID was originally assigned. This UUID can         
	// be used to lookup the organization record in the user registry service.                          
	//                                                                                                  
	// The UUID for the organization to which the CVE ID was originally assigned.                       
	AssignerOrgID                                                                               string  `json:"assignerOrgId"`
	// The short name for the organization to which the CVE ID was originally assigned.                 
	AssignerShortName                                                                           *string `json:"assignerShortName,omitempty"`
	// The CVE identifier that this record pertains to.                                                 
	CveID                                                                                       string  `json:"cveId"`
	// The date/time the CVE Record was first published in the CVE List.                                
	DatePublished                                                                               *string `json:"datePublished,omitempty"`
	// The date/time this CVE ID was reserved in the CVE automation workgroup services system.          
	// Disclaimer: This date reflects when the CVE ID was reserved, and does not necessarily            
	// indicate when this vulnerability was discovered, shared with the affected vendor,                
	// publicly disclosed, or updated in CVE.                                                           
	DateReserved                                                                                *string `json:"dateReserved,omitempty"`
	// The date/time the record was last updated.                                                       
	DateUpdated                                                                                 *string `json:"dateUpdated,omitempty"`
	// The user that requested the CVE identifier.                                                      
	RequesterUserID                                                                             *string `json:"requesterUserId,omitempty"`
	// The system of record causes this to start at 1, and increment by 1 each time a submission        
	// from a data provider changes this CVE Record. The incremented value moves to the Rejected        
	// schema upon a PUBLISHED->REJECTED transition, and moves to the Published schema upon a           
	// REJECTED->PUBLISHED transition.                                                                  
	Serial                                                                                      *int64  `json:"serial,omitempty"`
	// State of CVE - PUBLISHED, REJECTED.                                                              
	State                                                                                       State   `json:"state"`
	// The date/time the CVE ID was rejected.                                                           
	DateRejected                                                                                *string `json:"dateRejected,omitempty"`
}

// The default status for versions that are not otherwise listed in the versions list. If
// not specified, defaultStatus defaults to 'unknown'. Versions or defaultStatus may be
// omitted, but not both.
//
// The vulnerability status of a given version or range of versions of a product. The
// statuses 'affected' and 'unaffected' indicate that the version is affected or unaffected
// by the vulnerability. The status 'unknown' indicates that it is unknown or unspecified
// whether the given version is affected. There can be many reasons for an 'unknown' status,
// including that an investigation has not been undertaken or that a vendor has not
// disclosed the status.
//
// The new status in the range starting at the given version.
//
// The vulnerability status for the version or range of versions. For a range, the status
// may be refined by the 'changes' list.
type Status string

const (
	Affected   Status = "affected"
	Unaffected Status = "unaffected"
	Unknown    Status = "unknown"
)

// Type or role of the entity being credited (optional). finder: identifies the
// vulnerability.
// reporter: notifies the vendor of the vulnerability to a CNA.
// analyst: validates the vulnerability to ensure accuracy or severity.
// coordinator: facilitates the coordinated response process.
// remediation developer: prepares a code change or other remediation plans.
// remediation reviewer: reviews vulnerability remediation plans or code changes for
// effectiveness and completeness.
// remediation verifier: tests and verifies the vulnerability or its remediation.
// tool: names of tools used in vulnerability discovery or identification.
// sponsor: supports the vulnerability identification or remediation activities.
type Type string

const (
	Analyst              Type = "analyst"
	Coordinator          Type = "coordinator"
	Finder               Type = "finder"
	Other                Type = "other"
	RemediationDeveloper Type = "remediation developer"
	RemediationReviewer  Type = "remediation reviewer"
	RemediationVerifier  Type = "remediation verifier"
	Reporter             Type = "reporter"
	Sponsor              Type = "sponsor"
	Tool                 Type = "tool"
)

type AccessComplexityType string

const (
	AccessComplexityTypeHIGH   AccessComplexityType = "HIGH"
	AccessComplexityTypeLOW    AccessComplexityType = "LOW"
	AccessComplexityTypeMEDIUM AccessComplexityType = "MEDIUM"
)

type AccessVectorType string

const (
	AccessVectorTypeADJACENTNETWORK AccessVectorType = "ADJACENT_NETWORK"
	AccessVectorTypeLOCAL           AccessVectorType = "LOCAL"
	AccessVectorTypeNETWORK         AccessVectorType = "NETWORK"
)

type AuthenticationType string

const (
	AuthenticationTypeNONE AuthenticationType = "NONE"
	Multiple               AuthenticationType = "MULTIPLE"
	Single                 AuthenticationType = "SINGLE"
)

type CiaType string

const (
	CiaTypeNONE CiaType = "NONE"
	Complete    CiaType = "COMPLETE"
	Partial     CiaType = "PARTIAL"
)

type CiaRequirementType string

const (
	CiaRequirementTypeHIGH       CiaRequirementType = "HIGH"
	CiaRequirementTypeLOW        CiaRequirementType = "LOW"
	CiaRequirementTypeMEDIUM     CiaRequirementType = "MEDIUM"
	CiaRequirementTypeNOTDEFINED CiaRequirementType = "NOT_DEFINED"
)

type CollateralDamagePotentialType string

const (
	CollateralDamagePotentialTypeHIGH       CollateralDamagePotentialType = "HIGH"
	CollateralDamagePotentialTypeLOW        CollateralDamagePotentialType = "LOW"
	CollateralDamagePotentialTypeNONE       CollateralDamagePotentialType = "NONE"
	CollateralDamagePotentialTypeNOTDEFINED CollateralDamagePotentialType = "NOT_DEFINED"
	LowMedium                               CollateralDamagePotentialType = "LOW_MEDIUM"
	MediumHigh                              CollateralDamagePotentialType = "MEDIUM_HIGH"
)

type ExploitityType string

const (
	ExploitityTypeHIGH           ExploitityType = "HIGH"
	ExploitityTypeNOTDEFINED     ExploitityType = "NOT_DEFINED"
	ExploitityTypePROOFOFCONCEPT ExploitityType = "PROOF_OF_CONCEPT"
	Functional                   ExploitityType = "FUNCTIONAL"
	Unproven                     ExploitityType = "UNPROVEN"
)

type RemediationLevelType string

const (
	OfficialFix                    RemediationLevelType = "OFFICIAL_FIX"
	RemediationLevelTypeNOTDEFINED RemediationLevelType = "NOT_DEFINED"
	TemporaryFix                   RemediationLevelType = "TEMPORARY_FIX"
	Unavailable                    RemediationLevelType = "UNAVAILABLE"
	Workaround                     RemediationLevelType = "WORKAROUND"
)

type ReportConfidenceType string

const (
	ReportConfidenceTypeCONFIRMED  ReportConfidenceType = "CONFIRMED"
	ReportConfidenceTypeNOTDEFINED ReportConfidenceType = "NOT_DEFINED"
	Unconfirmed                    ReportConfidenceType = "UNCONFIRMED"
	Uncorroborated                 ReportConfidenceType = "UNCORROBORATED"
)

type TargetDistributionType string

const (
	TargetDistributionTypeHIGH       TargetDistributionType = "HIGH"
	TargetDistributionTypeLOW        TargetDistributionType = "LOW"
	TargetDistributionTypeMEDIUM     TargetDistributionType = "MEDIUM"
	TargetDistributionTypeNONE       TargetDistributionType = "NONE"
	TargetDistributionTypeNOTDEFINED TargetDistributionType = "NOT_DEFINED"
)

// CVSS Version
type CvssV20_Version string

const (
	The20 CvssV20_Version = "2.0"
)

type AttackComplexityType string

const (
	AttackComplexityTypeHIGH AttackComplexityType = "HIGH"
	AttackComplexityTypeLOW  AttackComplexityType = "LOW"
)

type JSONSchemaForCommonVulnerabilityScoringSystemVersion30_AttackVector string

const (
	AttackVectorTypeADJACENTNETWORK JSONSchemaForCommonVulnerabilityScoringSystemVersion30_AttackVector = "ADJACENT_NETWORK"
	PurpleLOCAL                     JSONSchemaForCommonVulnerabilityScoringSystemVersion30_AttackVector = "LOCAL"
	PurpleNETWORK                   JSONSchemaForCommonVulnerabilityScoringSystemVersion30_AttackVector = "NETWORK"
	PurplePHYSICAL                  JSONSchemaForCommonVulnerabilityScoringSystemVersion30_AttackVector = "PHYSICAL"
)

type AvailabilityImpactEnum string

const (
	TypeHIGH AvailabilityImpactEnum = "HIGH"
	TypeLOW  AvailabilityImpactEnum = "LOW"
	TypeNONE AvailabilityImpactEnum = "NONE"
)

type SeverityType string

const (
	Critical           SeverityType = "CRITICAL"
	SeverityTypeHIGH   SeverityType = "HIGH"
	SeverityTypeLOW    SeverityType = "LOW"
	SeverityTypeMEDIUM SeverityType = "MEDIUM"
	SeverityTypeNONE   SeverityType = "NONE"
)

type ModifiedAttackComplexityType string

const (
	ModifiedAttackComplexityTypeHIGH       ModifiedAttackComplexityType = "HIGH"
	ModifiedAttackComplexityTypeLOW        ModifiedAttackComplexityType = "LOW"
	ModifiedAttackComplexityTypeNOTDEFINED ModifiedAttackComplexityType = "NOT_DEFINED"
)

type JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedAttackVector string

const (
	FluffyLOCAL                             JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedAttackVector = "LOCAL"
	FluffyNETWORK                           JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedAttackVector = "NETWORK"
	FluffyPHYSICAL                          JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedAttackVector = "PHYSICAL"
	ModifiedAttackVectorTypeADJACENTNETWORK JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedAttackVector = "ADJACENT_NETWORK"
	PurpleNOTDEFINED                        JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedAttackVector = "NOT_DEFINED"
)

type ModifiedType string

const (
	ModifiedTypeHIGH       ModifiedType = "HIGH"
	ModifiedTypeLOW        ModifiedType = "LOW"
	ModifiedTypeNONE       ModifiedType = "NONE"
	ModifiedTypeNOTDEFINED ModifiedType = "NOT_DEFINED"
)

type ModifiedScopeType string

const (
	ModifiedScopeTypeCHANGED    ModifiedScopeType = "CHANGED"
	ModifiedScopeTypeNOTDEFINED ModifiedScopeType = "NOT_DEFINED"
	ModifiedScopeTypeUNCHANGED  ModifiedScopeType = "UNCHANGED"
)

type JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedUserInteraction string

const (
	FluffyNOTDEFINED                    JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedUserInteraction = "NOT_DEFINED"
	ModifiedUserInteractionTypeREQUIRED JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedUserInteraction = "REQUIRED"
	PurpleNONE                          JSONSchemaForCommonVulnerabilityScoringSystemVersion30_ModifiedUserInteraction = "NONE"
)

type ConfidenceType string

const (
	ConfidenceTypeCONFIRMED  ConfidenceType = "CONFIRMED"
	ConfidenceTypeNOTDEFINED ConfidenceType = "NOT_DEFINED"
	ConfidenceTypeUNKNOWN    ConfidenceType = "UNKNOWN"
	Reasonable               ConfidenceType = "REASONABLE"
)

type ScopeType string

const (
	ScopeTypeCHANGED   ScopeType = "CHANGED"
	ScopeTypeUNCHANGED ScopeType = "UNCHANGED"
)

type JSONSchemaForCommonVulnerabilityScoringSystemVersion30_UserInteraction string

const (
	FluffyNONE                  JSONSchemaForCommonVulnerabilityScoringSystemVersion30_UserInteraction = "NONE"
	UserInteractionTypeREQUIRED JSONSchemaForCommonVulnerabilityScoringSystemVersion30_UserInteraction = "REQUIRED"
)

// CVSS Version
type JSONSchemaForCommonVulnerabilityScoringSystemVersion30_Version string

const (
	The30 JSONSchemaForCommonVulnerabilityScoringSystemVersion30_Version = "3.0"
)

// CVSS Version
type JSONSchemaForCommonVulnerabilityScoringSystemVersion31_Version string

const (
	The31 JSONSchemaForCommonVulnerabilityScoringSystemVersion31_Version = "3.1"
)

type AttackRequirementsType string

const (
	AttackRequirementsTypeNONE    AttackRequirementsType = "NONE"
	AttackRequirementsTypePRESENT AttackRequirementsType = "PRESENT"
)

type JSONSchemaForCommonVulnerabilityScoringSystemVersion40_AttackVector string

const (
	AttackVectorTypeADJACENT JSONSchemaForCommonVulnerabilityScoringSystemVersion40_AttackVector = "ADJACENT"
	TentacledLOCAL           JSONSchemaForCommonVulnerabilityScoringSystemVersion40_AttackVector = "LOCAL"
	TentacledNETWORK         JSONSchemaForCommonVulnerabilityScoringSystemVersion40_AttackVector = "NETWORK"
	TentacledPHYSICAL        JSONSchemaForCommonVulnerabilityScoringSystemVersion40_AttackVector = "PHYSICAL"
)

type AutomatableType string

const (
	AutomatableTypeNOTDEFINED AutomatableType = "NOT_DEFINED"
	No                        AutomatableType = "NO"
	Yes                       AutomatableType = "YES"
)

type ExploitMaturityType string

const (
	Attacked                          ExploitMaturityType = "ATTACKED"
	ExploitMaturityTypeNOTDEFINED     ExploitMaturityType = "NOT_DEFINED"
	ExploitMaturityTypePROOFOFCONCEPT ExploitMaturityType = "PROOF_OF_CONCEPT"
	Unreported                        ExploitMaturityType = "UNREPORTED"
)

type ModifiedAttackRequirementsType string

const (
	ModifiedAttackRequirementsTypeNONE       ModifiedAttackRequirementsType = "NONE"
	ModifiedAttackRequirementsTypeNOTDEFINED ModifiedAttackRequirementsType = "NOT_DEFINED"
	ModifiedAttackRequirementsTypePRESENT    ModifiedAttackRequirementsType = "PRESENT"
)

type JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedAttackVector string

const (
	ModifiedAttackVectorTypeADJACENT JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedAttackVector = "ADJACENT"
	StickyLOCAL                      JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedAttackVector = "LOCAL"
	StickyNETWORK                    JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedAttackVector = "NETWORK"
	StickyPHYSICAL                   JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedAttackVector = "PHYSICAL"
	TentacledNOTDEFINED              JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedAttackVector = "NOT_DEFINED"
)

type ModifiedSubIaType string

const (
	ModifiedSubIaTypeHIGH       ModifiedSubIaType = "HIGH"
	ModifiedSubIaTypeLOW        ModifiedSubIaType = "LOW"
	ModifiedSubIaTypeNONE       ModifiedSubIaType = "NONE"
	ModifiedSubIaTypeNOTDEFINED ModifiedSubIaType = "NOT_DEFINED"
	Safety                      ModifiedSubIaType = "SAFETY"
)

type JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedUserInteraction string

const (
	ModifiedUserInteractionTypeACTIVE  JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedUserInteraction = "ACTIVE"
	ModifiedUserInteractionTypePASSIVE JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedUserInteraction = "PASSIVE"
	StickyNOTDEFINED                   JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedUserInteraction = "NOT_DEFINED"
	TentacledNONE                      JSONSchemaForCommonVulnerabilityScoringSystemVersion40_ModifiedUserInteraction = "NONE"
)

type ProviderUrgencyType string

const (
	Amber                         ProviderUrgencyType = "AMBER"
	Clear                         ProviderUrgencyType = "CLEAR"
	Green                         ProviderUrgencyType = "GREEN"
	ProviderUrgencyTypeNOTDEFINED ProviderUrgencyType = "NOT_DEFINED"
	Red                           ProviderUrgencyType = "RED"
)

type RecoveryType string

const (
	Automatic              RecoveryType = "AUTOMATIC"
	Irrecoverable          RecoveryType = "IRRECOVERABLE"
	RecoveryTypeNOTDEFINED RecoveryType = "NOT_DEFINED"
	User                   RecoveryType = "USER"
)

type SafetyType string

const (
	Negligible           SafetyType = "NEGLIGIBLE"
	SafetyTypeNOTDEFINED SafetyType = "NOT_DEFINED"
	SafetyTypePRESENT    SafetyType = "PRESENT"
)

type JSONSchemaForCommonVulnerabilityScoringSystemVersion40_UserInteraction string

const (
	StickyNONE                 JSONSchemaForCommonVulnerabilityScoringSystemVersion40_UserInteraction = "NONE"
	UserInteractionTypeACTIVE  JSONSchemaForCommonVulnerabilityScoringSystemVersion40_UserInteraction = "ACTIVE"
	UserInteractionTypePASSIVE JSONSchemaForCommonVulnerabilityScoringSystemVersion40_UserInteraction = "PASSIVE"
)

type ValueDensityType string

const (
	Concentrated               ValueDensityType = "CONCENTRATED"
	Diffuse                    ValueDensityType = "DIFFUSE"
	ValueDensityTypeNOTDEFINED ValueDensityType = "NOT_DEFINED"
)

// CVSS Version
type JSONSchemaForCommonVulnerabilityScoringSystemVersion40_Version string

const (
	The40 JSONSchemaForCommonVulnerabilityScoringSystemVersion40_Version = "4.0"
)

type VulnerabilityResponseEffortType string

const (
	Moderate                                  VulnerabilityResponseEffortType = "MODERATE"
	VulnerabilityResponseEffortTypeHIGH       VulnerabilityResponseEffortType = "HIGH"
	VulnerabilityResponseEffortTypeLOW        VulnerabilityResponseEffortType = "LOW"
	VulnerabilityResponseEffortTypeNOTDEFINED VulnerabilityResponseEffortType = "NOT_DEFINED"
)

// State of CVE - PUBLISHED, REJECTED.
type State string

const (
	Published State = "PUBLISHED"
	Rejected  State = "REJECTED"
)

// Indicates the type of information represented in the JSON instance.
type DataType string

const (
	CveRecord DataType = "CVE_RECORD"
)
