package cli_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"regexp"
	"testing"

	"gitlab.com/mhenriksen/gittest"

	"gitlab.com/gitlab-org/cves/internal/cli"
	"gitlab.com/gitlab-org/cves/internal/cve"
)

const (
	testCNAUUID      = "832e23b5-c96b-4bdc-9886-6ac93b363a5b"
	testCNAShortname = "GitLab"
	testAPIUser      = "cve@gitlab.test"
	testAPIKey       = "deadbeefdeadbeef"
)

func Test_Mitresync_OnlyAddedRecords(t *testing.T) {
	r := gittest.New(t)

	commitFile(t, r, testdata(t, "records", "CVE-2024-0199.json"), "2024/CVE-2024-0199.json")
	commitFile(t, r, testdata(t, "records", "CVE-2024-0207.json"), "2024/CVE-2024-0207.json")

	commits := r.GetCommits()

	t.Setenv("CI_COMMIT_BEFORE_SHA", commits[1])

	out, exitCode := runMitresync(t, r.Dir())

	if exitCode != 0 {
		t.Errorf("expected exit code 0 but got %d", exitCode)
	}

	requireOutMatch(t, out, regexp.MustCompile(`no modified records to update on MITRE`))
}

func Test_Mitresync_ModifiedRecords(t *testing.T) {
	mux := http.NewServeMux()
	mux.Handle("/cve/CVE-2024-0207", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			t.Errorf("expected request method %s for %s but was %s", http.MethodGet, r.URL.Path, r.Method)
			return
		}

		requireAPIHeaders(t, r)

		w.Write(testdata(t, "records", "CVE-2024-0207.json"))
	}))
	mux.Handle("/cve/CVE-2024-0207/cna", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPut {
			t.Errorf("expected request method %s for %s but was %s", http.MethodGet, r.URL.Path, r.Method)
			return
		}

		requireAPIHeaders(t, r)

		body := struct {
			CNAContainer *cve.CnaEdContainer `json:"cnaContainer,omitEmpty"`
		}{}

		if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
			t.Errorf("expected no error decoding request body but got: %v", err)
			return
		}

		if body.CNAContainer == nil {
			t.Errorf("expected request body to contain CNA container")
			return
		}

		if body.CNAContainer.Metrics[0].CvssV31.BaseScore != 7.0 {
			t.Errorf("expected CNA container to contain CVSS base score of 7.0")
		}
	}))

	server := httptest.NewServer(mux)
	defer server.Close()

	r := gittest.New(t)

	commitFile(t, r, testdata(t, "records", "CVE-2024-0199.json"), "2024/CVE-2024-0199.json")
	commitFile(t, r, testdata(t, "records", "CVE-2024-0207.json"), "2024/CVE-2024-0207.json")
	commitFile(t, r, testdata(t, "records", "CVE-2024-0208.json"), "2024/CVE-2024-0208.json")

	r.Write(filepath.Join("2024/CVE-2024-0207.json"), testdata(t, "records", "CVE-2024-0207-updated-cvss.json"), 0o644).
		Git("commit", "-am", "Update CVE-2024-0207")

	commits := r.GetCommits()

	t.Setenv("CI_COMMIT_BEFORE_SHA", commits[2])

	out, exitCode := runMitresync(t, "--mitre-base-url", server.URL, r.Dir())

	if exitCode != 0 {
		t.Errorf("expected exit code 0 but got %d", exitCode)
	}

	requireOutMatch(t, out, regexp.MustCompile(`updated modified record on MITRE record=CVE-2024-0207`))
	requireOutMatch(t, out, regexp.MustCompile(`finished with no errors`))
}

func Test_Mitresync_OtherModifiedFiles(t *testing.T) {
	r := gittest.New(t)

	commitFile(t, r, testdata(t, "records", "CVE-2024-0199.json"), "2024/CVE-2024-0199.json")
	commitFile(t, r, testdata(t, "records", "CVE-2024-0207.json"), "2024/CVE-2024-0207.json")
	commitFile(t, r, testdata(t, "records", "CVE-2024-0207.json"), "2024/WAT-2024-0207.json")
	commitFile(t, r, testdata(t, "records", "CVE-2024-0208.json"), "2024/CVE-2024-0208.json")

	r.Write(filepath.Join("2024/WAT-2024-0207.json"), testdata(t, "records", "CVE-2024-0207-updated-cvss.json"), 0o644).
		Git("commit", "-am", "Update WAT-2024-0207")

	commits := r.GetCommits()

	t.Setenv("CI_COMMIT_BEFORE_SHA", commits[3])

	out, exitCode := runMitresync(t, r.Dir())

	if exitCode != 0 {
		t.Errorf("expected exit code 0 but got %d", exitCode)
	}

	requireOutMatch(t, out, regexp.MustCompile(`no modified records to update on MITRE`))
}

func Test_Mitresync_ModifiedRecordNotCNARecord(t *testing.T) {
	mux := http.NewServeMux()
	mux.Handle("/cve/CVE-2024-0199", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			t.Errorf("expected request method %s for %s but was %s", http.MethodGet, r.URL.Path, r.Method)
			return
		}

		requireAPIHeaders(t, r)

		w.Write(testdata(t, "records", "CVE-2024-0199.json"))
	}))

	server := httptest.NewServer(mux)
	defer server.Close()

	r := gittest.New(t)

	commitFile(t, r, testdata(t, "records", "CVE-2024-0199.json"), "2024/CVE-2024-0199.json")
	commitFile(t, r, testdata(t, "records", "CVE-2024-0207.json"), "2024/CVE-2024-0207.json")
	commitFile(t, r, testdata(t, "records", "CVE-2024-0208.json"), "2024/CVE-2024-0208.json")

	r.Write(filepath.Join("2024/CVE-2024-0199.json"), testdata(t, "records", "CVE-2024-0199-updated-assigner-shortname.json"), 0o644).
		Git("commit", "-am", "Update CVE-2024-0199")

	commits := r.GetCommits()

	t.Setenv("CI_COMMIT_BEFORE_SHA", commits[3])

	out, exitCode := runMitresync(t, "-mitre-base-url", server.URL, r.Dir())

	if exitCode != 0 {
		t.Errorf("expected exit code 0 but got %d", exitCode)
	}

	requireOutMatch(t, out, regexp.MustCompile(`record has no changes to CNA container; skipping record=CVE-2024-0199`))
}

func Test_Mitresync_ModifiedRecordSameAsMITRE(t *testing.T) {
	mux := http.NewServeMux()
	mux.Handle("/cve/CVE-2024-0208", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			t.Errorf("expected request method %s for %s but was %s", http.MethodGet, r.URL.Path, r.Method)
			return
		}

		requireAPIHeaders(t, r)

		w.Write(testdata(t, "records", "CVE-2024-0208.json"))
	}))

	server := httptest.NewServer(mux)
	defer server.Close()

	r := gittest.New(t)

	commitFile(t, r, testdata(t, "records", "CVE-2024-0199.json"), "2024/CVE-2024-0199.json")
	commitFile(t, r, testdata(t, "records", "CVE-2024-0207.json"), "2024/CVE-2024-0207.json")
	commitFile(t, r, []byte("{}"), "2024/CVE-2024-0208.json")

	r.Write(filepath.Join("2024/CVE-2024-0208.json"), testdata(t, "records", "CVE-2024-0208.json"), 0o644).
		Git("commit", "-am", "Update CVE-2024-0208")

	commits := r.GetCommits()

	t.Setenv("CI_COMMIT_BEFORE_SHA", commits[1])

	out, exitCode := runMitresync(t, "-mitre-base-url", server.URL, r.Dir())

	if exitCode != 0 {
		t.Errorf("expected exit code 0 but got %d", exitCode)
	}

	requireOutMatch(t, out, regexp.MustCompile(`record has no changes to CNA container; skipping record=CVE-2024-0208`))
}

func runMitresync(tb testing.TB, args ...string) ([]byte, int) {
	tb.Helper()

	tb.Setenv("TEST_MITRESYNC", "1")
	tb.Setenv("MITRE_CNA_UUID", testCNAUUID)
	tb.Setenv("MITRE_CNA_SHORTNAME", testCNAShortname)
	tb.Setenv("MITRE_API_USER", testAPIUser)
	tb.Setenv("MITRE_API_KEY", testAPIKey)

	out := &bytes.Buffer{}
	exitCode := cli.MitreSync(out, args)

	return out.Bytes(), exitCode
}

func testdata(tb testing.TB, path ...string) []byte {
	tb.Helper()

	name := filepath.Join(append([]string{"testdata"}, path...)...)

	data, err := os.ReadFile(name)
	if err != nil {
		tb.Fatalf("reading testdata file %s: %v", name, err)
	}

	return data
}

func commitFile(tb testing.TB, r *gittest.Repository, data []byte, path ...string) {
	tb.Helper()

	name := filepath.Join(path...)

	r.Write(name, data, 0o644).
		Git("add", name).
		Git("commit", "-m", filepath.Base(name))
}

func requireOutMatch(tb testing.TB, out []byte, re *regexp.Regexp) {
	tb.Helper()

	if !re.Match(out) {
		tb.Errorf("expected output to match `%v`\n\nOUTPUT:\n\n%s", re, out)
	}
}

func requireAPIHeaders(tb testing.TB, r *http.Request) {
	tb.Helper()

	wantHeaders := map[string]string{
		"CVE-API-ORG":  testCNAShortname,
		"CVE-API-USER": testAPIUser,
		"CVE-API-KEY":  testAPIKey,
	}

	for k, want := range wantHeaders {
		actual := r.Header.Get(k)
		if actual != want {
			tb.Errorf("expected request %s %s to have header %s with value %q but was %q", r.Method, r.URL.Path, k, want, actual)
		}
	}
}
